#This file is part of the account_voucher_ar module for Tryton.
#The COPYRIGHT file at the top level of this repository contains
#the full copyright notices and license terms.
from trytond.pool import Pool
import voucher
import account
import configuration
import invoice
import multirevenue


def register():
    Pool.register(
        voucher.VoucherPayMode,
        invoice.Invoice,
        voucher.Voucher,
        voucher.VoucherLine,
        voucher.SelectLinesAsk,
        account.Move,
        configuration.VoucherConfiguration,
        configuration.VoucherSequence,
        voucher.Note,
        voucher.NoteLine,
        voucher.VoucherTemplate,
        voucher.TemplateLine,
        invoice.AdvancePaymentAsk,
        voucher.VoucherFixNumberStart,
        voucher.NoteFixNumberStart,
        voucher.SelectMoveLinesAsk,
        voucher.VoucherTemplateParty,
        voucher.VoucherTemplateAccount,
        voucher.CreateVoucherStart,
        voucher.VoucherSheetStart,
        invoice.CrossPaymentAsk,
        voucher.AddZeroAdjustmentStart,
        voucher.TaxesConsolidationStart,
        voucher.TaxesConsolidationDone,
        voucher.ReceiptRelation,
        voucher.AdvanceVoucherStart,
        multirevenue.MultiRevenue,
        multirevenue.MultiRevenueTransaction,
        multirevenue.MultiRevenueLine,
        module='account_voucher', type_='model')
    Pool.register(
        voucher.SelectLines,
        invoice.AdvancePayment,
        voucher.VoucherFixNumber,
        voucher.NoteFixNumber,
        voucher.SelectMoveLines,
        voucher.CreateVoucher,
        voucher.VoucherSheet,
        invoice.CrossPayment,
        voucher.AddZeroAdjustment,
        voucher.TaxesConsolidation,
        voucher.AdvanceVoucher,
        multirevenue.PaymentLines,
        module='account_voucher', type_='wizard')
    Pool.register(
        voucher.VoucherReport,
        voucher.NoteReport,
        voucher.FilteredVouchersReport,
        voucher.VoucherMoveReport,
        voucher.VoucherSheetReport,
        # voucher.ReceiptRelationReport,
        module='account_voucher', type_='report')
