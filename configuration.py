# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelSQL, ModelView, fields
from trytond.pyson import Eval
from trytond.transaction import Transaction


__all__ = ['VoucherConfiguration', 'VoucherSequence']


class VoucherConfiguration(ModelSQL, ModelView):
    'Voucher Configuration'
    __name__ = 'account.voucher_configuration'
    company = fields.Many2One('company.company', 'Company', required=True)
    default_journal_note = fields.Many2One('account.journal', 'Default Journal Note')
    default_payment_mode = fields.Many2One('account.voucher.paymode',
        'Default Payment Mode', domain=[
            ('company', 'in', [Eval('company', -1), None]),
        ])
    account_adjust_expense = fields.Many2One('account.account',
        'Account Adjustment Zero for Expense', domain=[
            ('type', '!=', 'view'),
        ])
    account_adjust_income = fields.Many2One('account.account',
        'Account Adjustment Zero for Income', domain=[
            ('type', '!=', 'view'),
        ])
    voucher_notes_sequence = fields.Many2One('ir.sequence',
        'Voucher Notes Sequence', required=False,
        domain=[
            ('company', 'in', [Eval('company', -1), None]),
            ('code', '=', 'account.voucher')
        ])
    multirevenue_sequence = fields.Many2One('ir.sequence',
        'Multi-Revenue Sequence', required=False,
        domain=[
            ('company', 'in', [Eval('company', -1), None]),
            ('code', '=', 'account.multirevenue')
        ])
    account_prepayment = fields.Many2One('account.account', 'Account Prepayment',
            required=True, domain=[
                ('reconcile', '=', True),
                ('kind', 'in', ['receivable', 'payable']),
            ])
    prepayment_description = fields.Char('Prepayment Description',
        help="Default descriptor for advances")

    @classmethod
    def __setup__(cls):
        super(VoucherConfiguration, cls).__setup__()
        cls._error_messages.update({
            'missing_default_configuration': (
                'Missing the configuration for current company!'),
        })

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @classmethod
    def get_configuration(cls):
        res = cls.search([
            ('company', '=', Transaction().context.get('company'))
        ])
        if res:
            return res[0]
        else:
            cls.raise_user_error('missing_default_configuration')


class VoucherSequence(ModelSQL, ModelView):
    'Voucher Sequence'
    __name__ = 'account.voucher.sequence'
    voucher_receipt_sequence = fields.MultiValue(fields.Many2One('ir.sequence',
        'Voucher Receipt Sequence', required=True,
        domain=[('code', '=', 'account.voucher')]))
    voucher_payment_sequence = fields.MultiValue(fields.Many2One('ir.sequence',
        'Voucher Payment Sequence', required=True,
        domain=[('code', '=', 'account.voucher')]))
    voucher_notes_sequence = fields.MultiValue(fields.Many2One('ir.sequence',
        'Voucher Notes Sequence', required=True,
        domain=[('code', '=', 'account.voucher')]))
    voucher_multipayment_sequence = fields.MultiValue(fields.Many2One('ir.sequence',
        'Voucher Multipayment Sequence', required=True,
        domain=[('code', '=', 'account.voucher')]))
    voucher_multirevenue_sequence = fields.MultiValue(fields.Many2One('ir.sequence',
        'Voucher Multirevenue Sequence', required=True,
        domain=[('code', '=', 'account.multirevenue')]))
