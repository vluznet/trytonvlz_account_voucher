# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from datetime import date
from decimal import Decimal

from trytond.model import ModelView, fields
from trytond.pool import Pool, PoolMeta
from trytond.wizard import Wizard, StateTransition, StateView, Button
from trytond.transaction import Transaction
from trytond.pyson import Eval

__all__ = ['Invoice', 'AdvancePaymentAsk', 'AdvancePayment',
    'CrossPaymentAsk', 'CrossPayment']

_ZERO = Decimal('0.0')


class Invoice(metaclass=PoolMeta):
    __name__ = 'account.invoice'

    @classmethod
    def post(cls, invoices):
            super(Invoice, cls).post(invoices)
            for invoice in invoices:
                for line in invoice.lines:
                    if line.origin and hasattr(line.origin, 'sale'):
                        if line.origin.sale.vouchers:
                            invoice.create_move_advance(line.origin.sale.vouchers)
                            break


    def create_move_advance(self, vouchers):
        pool = Pool()
        Note = pool.get('account.note')
        Invoice = pool.get('account.invoice')
        MoveLine = pool.get('account.move.line')
        # invoice = Invoice(Transaction().context.get('active_id'))
        Config = pool.get('account.voucher_configuration')
        config = Config.get_configuration()

        for voucher in vouchers:
            if voucher.state in ['posted', 'processed'] and voucher.move:
                line_ids = [line.id for line in voucher.move.lines]
                move_lines = MoveLine.search([
                    ('account.kind', 'in', ['payable', 'receivable']),
                    ('id', 'in', line_ids),
                ])

                lines_to_create = []
                accounts_to_reconcile = []

                reconcile_invoice = []

                sum_debit = 0
                sum_credit = 0
                for line in move_lines:
                    lines_to_create.append({
                        'debit': line.credit,
                        'credit': line.debit,
                        'party': line.party.id,
                        'account': line.account.id,
                        'description': line.description,
                    })
                    sum_debit += line.debit
                    sum_credit += line.credit
                    accounts_to_reconcile.append(line.account.id)

                total_advance = abs(sum_debit - sum_credit)
                if total_advance == self.amount_to_pay:
                    for mline in self.move.lines:
                        if mline.account.id == self.account.id:
                            reconcile_invoice.append(mline)

                for pl in self.payment_lines:
                    if not pl.reconciliation:
                        reconcile_invoice.append(pl)

                lines_to_create.append({
                    'debit': sum_debit,
                    'credit': sum_credit,
                    'party': self.party.id,
                    'account': self.account.id,
                    'description': self.description,
                })

                note, = Note.create([{
                    'description': '',
                    'journal': config.default_journal_note.id,
                    'date': date.today(),
                    'state': 'draft',
                    'lines': [('create', lines_to_create)],
                }])
                Note.post([note])

                lines_to_reconcile = list(move_lines)
                payment_lines = []
                for nm_line in note.move.lines:
                    if nm_line.account.id == self.account.id:
                        print(self.total_amount, nm_line.account.code, nm_line.debit, nm_line.credit)
                        payment_lines.append(nm_line)
                        if total_advance == self.amount_to_pay:
                            reconcile_invoice.append(nm_line)
                    if nm_line.account.id in accounts_to_reconcile:
                        lines_to_reconcile.append(nm_line)
                Invoice.write([self], {
                    'payment_lines': [('add', payment_lines)],
                })

                if lines_to_reconcile:
                    MoveLine.reconcile(lines_to_reconcile)

                pending_to_pay = sum([ri.debit - ri.credit for ri in reconcile_invoice])
                if reconcile_invoice and not pending_to_pay:
                    for x in reconcile_invoice:
                        MoveLine.reconcile(reconcile_invoice)

    @classmethod
    def __setup__(cls):
        super(Invoice, cls).__setup__()
        cls._buttons.update({
            'pay_with_voucher': {
                'invisible': Eval('state') != 'posted',
                'depends': ['state'],
            }
        },)

    @classmethod
    @ModelView.button_action('account_voucher.act_pay_with_voucher')
    def pay_with_voucher(cls, invoices):
        cls.create_voucher(invoices)

    @classmethod
    def create_voucher(cls, invoices):
        pool = Pool()
        Voucher = pool.get('account.voucher')
        Config = pool.get('account.voucher_configuration')
        config = Config.get_configuration()
        PayMode = pool.get('account.voucher.paymode')
        Model = pool.get('ir.model')

        today = date.today()
        payment_mode = None
        model, = Model.search([
            ('model', '=', cls.__name__)
        ])

        if config.default_payment_mode:
            payment_mode = config.default_payment_mode
        else:
            values = PayMode.search([
                ('company', '=', Transaction().context.get('company'))
            ])
            if values:
                payment_mode = values[0]
        if not payment_mode:
            return Voucher.raise_user_error('missing_paymode')

        total_amount_to_pay = sum([i.amount_to_pay for i in invoices])
        if not invoices:
            return
        party, type_ = invoices[0].party, invoices[0].type

        if type_ == 'in':
            if total_amount_to_pay < _ZERO:
                voucher_type = 'receipt'
            else:
                voucher_type = 'payment'
        else:
            if total_amount_to_pay > _ZERO:
                voucher_type = 'receipt'
            else:
                voucher_type = 'payment'

        account_id = Voucher.get_account(voucher_type, payment_mode)

        voucher_to_create = {
            'party': party.id,
            'voucher_type': voucher_type,
            'date': today,
            'description': '',
            'payment_mode': payment_mode.id,
            'state': 'draft',
            'account': account_id,
            'journal': payment_mode.journal.id,
            'lines': [('create', [])],
            'method_counterpart': 'one_line',
        }
        if payment_mode.bank_account and payment_mode.bank_account:
            voucher_to_create['bank'] = payment_mode.bank_account.bank.id

        for invoice in invoices:
            if invoice.party.id != party.id:
                continue
            if invoice.state != 'posted' or not invoice.move \
                or invoice.reconciled:
                continue
            sign = 1
            if total_amount_to_pay < _ZERO and invoice.amount_to_pay < _ZERO:
                sign = (-1)

            for move_line in invoice.move.lines:
                if move_line.account.id != invoice.account.id or move_line.reconciliation:
                    continue

                detail = (model.name + ' ' + invoice.number)

                voucher_to_create['lines'][0][1].append({
                    'detail': detail,
                    'amount': invoice.amount_to_pay * sign,
                    'amount_original': invoice.total_amount * sign,
                    'move_line': move_line.id,
                    'account': move_line.account.id,
                })

        vouchers = Voucher.create([voucher_to_create])
        for voucher in vouchers:
            voucher.on_change_lines()
            voucher.save()
        return vouchers


class AdvancePaymentAsk(ModelView):
    'Advance Payment Ask'
    __name__ = 'account.invoice.advance_payment.ask'
    lines = fields.Many2Many('account.move.line', None, None,
            'Account Moves Lines')


class AdvancePayment(Wizard):
    'Advance Payment'
    __name__ = 'account.invoice.advance_payment'
    start_state = 'search_advance'
    search_advance = StateTransition()
    start = StateView('account.invoice.advance_payment.ask',
            'account_voucher.view_search_advance_payment_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Add', 'add_link', 'tryton-ok', default=True),
        ])
    add_link = StateTransition()

    @classmethod
    def __setup__(cls):
        super(AdvancePayment, cls).__setup__()
        cls._error_messages.update({
            'invalid_total_advance': 'Total payment advances can not '
            'be greater than Amount to Pay on invoice!',
        })

    def transition_search_advance(self):
        pool = Pool()
        Invoice = pool.get('account.invoice')
        Advance = pool.get('account.invoice.advance_payment.ask')
        invoice = Invoice(Transaction().context.get('active_id'))
        if invoice.type == 'in':
            account_types = ['receivable']
            debit_credit = ('debit', '>', 0)
        elif invoice.type == 'out':
            account_types = ['payable']
            debit_credit = ('credit', '>', 0)
        else:
            return 'end'
        domain = [
            ('reconciliation', '=', None),
            ('move.state', '=', 'posted'),
            ('party', '=', invoice.party.id),
            ('state', '=', 'valid'),
            ('account.kind', 'in', account_types),
        ]
        domain.append(debit_credit)
        Advance.lines.domain = domain
        return 'start'

    def transition_add_link(self):
        pool = Pool()
        Note = pool.get('account.note')
        Invoice = pool.get('account.invoice')
        MoveLine = pool.get('account.move.line')
        invoice = Invoice(Transaction().context.get('active_id'))
        Config = pool.get('account.voucher_configuration')
        config = Config.get_configuration()

        move_lines = self.start.lines
        if not config.default_journal_note:
            Note.raise_user_error('missing_journal_note')

        print('1')
        lines_to_create = []
        accounts_to_reconcile = []

        reconcile_invoice = []

        sum_debit = 0
        sum_credit = 0
        for line in move_lines:
            lines_to_create.append({
                'debit': line.credit,
                'credit': line.debit,
                'party': line.party.id,
                'account': line.account.id,
                'description': line.description,
            })
            sum_debit += line.debit
            sum_credit += line.credit
            accounts_to_reconcile.append(line.account.id)

        total_advance = abs(sum_debit - sum_credit)
        if total_advance > invoice.amount_to_pay:
            self.raise_user_error('invalid_total_advance')
        elif total_advance == invoice.amount_to_pay:
            for mline in invoice.move.lines:
                if mline.account.id == invoice.account.id:
                    reconcile_invoice.append(mline)

        for pl in invoice.payment_lines:
            if not pl.reconciliation:
                reconcile_invoice.append(pl)
        print('2')
        lines_to_create.append({
            'debit': sum_debit,
            'credit': sum_credit,
            'party': invoice.party.id,
            'account': invoice.account.id,
            'description': invoice.description,
        })

        note, = Note.create([{
            'description': '',
            'journal': config.default_journal_note.id,
            'date': date.today(),
            'state': 'draft',
            'lines': [('create', lines_to_create)],
        }])
        Note.post([note])
        print('4')
        lines_to_reconcile = list(move_lines)
        payment_lines = []
        for nm_line in note.move.lines:
            if nm_line.account.id == invoice.account.id:
                payment_lines.append(nm_line)
                if total_advance == invoice.amount_to_pay:
                    reconcile_invoice.append(nm_line)
            if nm_line.account.id in accounts_to_reconcile:
                lines_to_reconcile.append(nm_line)

        Invoice.write([invoice], {
            'payment_lines': [('add', payment_lines)],
        })

        print('5')
        if lines_to_reconcile:
            MoveLine.reconcile(lines_to_reconcile)

        print('6')

        pending_to_pay = sum([ri.debit - ri.credit for ri in reconcile_invoice])
        if reconcile_invoice and not pending_to_pay:
            for x in reconcile_invoice:
                print(x.id, x.description, x.debit, x.credit)
            MoveLine.reconcile(reconcile_invoice)
        print('7')
        return 'end'


class CrossPaymentAsk(ModelView):
    'Cross Payment Ask'
    __name__ = 'account.invoice.cross_payment.ask'
    account = fields.Many2One('account.account', 'Account', required=True)
    amount = fields.Numeric('Amount', digits=(16, 2), required=True)
    date = fields.Date('Date', required=True)
    description = fields.Char('Description')


class CrossPayment(Wizard):
    'Advance Payment'
    __name__ = 'account.invoice.cross_payment'
    start = StateView('account.invoice.cross_payment.ask',
        'account_voucher.view_search_cross_payment_form', [
        Button('Cancel', 'end', 'tryton-cancel'),
        Button('Add', 'add_payment', 'tryton-ok', default=True),
    ])
    add_payment = StateTransition()

    @classmethod
    def __setup__(cls):
        super(CrossPayment, cls).__setup__()
        cls._error_messages.update({
            'invalid_total_cross': 'Total payment crosss can not '
            'be greater than Amount to Pay on invoice!',
        })

    def do_add_payment(self, action):
        data = {
            'account': self.start.account.id,
            'amount': self.start.amount,
            'date': self.start.date,
            'description': self.start.description,
        }
        return action, data

    def transition_add_payment(self):
        pool = Pool()
        Note = pool.get('account.note')
        Invoice = pool.get('account.invoice')
        Period = pool.get('account.period')
        Move = pool.get('account.move')
        MoveLine = pool.get('account.move.line')
        invoice = Invoice(Transaction().context.get('active_id'))
        Config = pool.get('account.voucher_configuration')
        config = Config.get_configuration()

        if not config.default_journal_note:
            Note.raise_user_error('missing_journal_note')

        lines_to_create = []
        accounts_to_reconcile = []
        company_id = Transaction().context.get('company')
        period_id = Period.find(company_id, date=self.start.date)
        move_to_create = {
            'date': self.start.date,
            'period': period_id,
            'description': self.start.description,
            'journal': config.default_journal_note.id,
            'state': 'draft',
        }

        move, = Move.create([move_to_create])
        lines_to_create.append({
            'move': move.id,
            'debit': _ZERO,
            'credit': self.start.amount,
            'party': invoice.party.id,
            'account': self.start.account.id,
            'description': self.start.description,
        })
        lines_to_create.append({
            'move': move.id,
            'debit': self.start.amount,
            'credit': _ZERO,
            'party': invoice.party.id,
            'account': invoice.account.id,
            'description': self.start.description,
        })
        pay_lines = MoveLine.create(lines_to_create)

        accounts_to_reconcile.append(invoice.account.id)
        """
        total_cross = abs(sum_debit - sum_credit)
        if total_cross > invoice.amount_to_pay:
            self.raise_user_error('invalid_total_cross')
        elif total_cross == invoice.amount_to_pay:
            for mline in invoice.move.lines:
                if mline.account.id == invoice.account.id:
                    reconcile_invoice.append(mline)
        else:
            pass
        """

        Move.post([move])
        lines_to_reconcile = []
        payment_lines = []

        for move_line in invoice.move.lines:
            if move_line.account.id == invoice.account.id:
                lines_to_reconcile.append(move_line)

        for pay_line in pay_lines:
            if pay_line.account.id in accounts_to_reconcile:
                payment_lines.append(pay_line)

        Invoice.write([invoice], {
            'payment_lines': [('add', payment_lines)],
        })

        """
        if lines_to_reconcile:
            MoveLine.reconcile(lines_to_reconcile)
        if reconcile_invoice:
            MoveLine.reconcile(reconcile_invoice)
        """
        return 'end'
