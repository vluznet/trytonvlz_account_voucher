# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from datetime import date
from decimal import Decimal
from sql import Table
from trytond import backend
from trytond.model import ModelView, ModelSQL, fields, Workflow
from trytond.wizard import Wizard, StateView, StateTransition, Button, StateReport
from trytond.transaction import Transaction
from trytond.pyson import Eval, In, Or, Bool, Get, If, Not
from trytond.pool import Pool
from trytond.report import Report


conversor = None
try:
    from numword import numword_es
    conversor = numword_es.NumWordES()
except:
    print("Warning: Does not possible import numword module, please install it...!")



__all__ = ['MultiRevenue', 'PaymentLines', 'MultiRevenueTransaction', 'MultiRevenueLine']

STATES = {
    'readonly': Eval('state') != 'draft',
}


class MultiRevenue(Workflow, ModelSQL, ModelView):
    'MultiRevenue'
    __name__ = 'account.multirevenue'
    _rec_name = 'code'
    code = fields.Char('Code', readonly=True)
    date = fields.Date('Date', required=True, states=STATES)
    company = fields.Many2One('company.company', 'Company', required=True,  states=STATES)
    party = fields.Many2One('party.party', 'Party', required=True,  states=STATES)
    transactions = fields.One2Many('account.multirevenue.transaction', 'multirevenue', 'Transactions',  states=STATES)
    state = fields.Selection([
            ('draft', 'Draft'),
            ('processed', 'Processed'),
            ('cancel', 'Cancel'),
            ], 'State', select=True, readonly=True)
    total_transaction = fields.Function(fields.Numeric('Total Transaction', readonly=True), 'get_total_transaction')
    total_lines_to_pay = fields.Function(fields.Numeric('Total Lines to Pay', readonly=True), 'get_total_amount_line')
    new_balance = fields.Function(fields.Numeric('New Balance', readonly=True), 'get_new_balance')
    total_line_receipt = fields.Function(fields.Numeric('Total Line Receipt', readonly=True), 'get_total_line_receipt')
    lines = fields.One2Many('account.multirevenue.line', 'multirevenue', 'Lines',  states=STATES)

    @classmethod
    def __setup__(cls):
        super(MultiRevenue, cls).__setup__()
        cls._error_messages.update({
                'exist_voucher': 'You cannot perform the operation because already exist vouchers generated!',
                'without_account_prepayment': 'Not exist account for prepayments in the configuration of vouchers!',
        })
        cls._buttons.update({
            'draft': {
                'invisible': Eval('state') == 'draft',
                },
            'process': {
                'invisible': Eval('state') != 'draft',
                },
            'cancel': {
                'invisible': Eval('state') != 'draft',
            },
        })
        cls._transitions |= set((
                ('draft', 'processed'),
                ('draft', 'cancel'),
                ('processed', 'draft'),
                ('processed', 'posted'),
                ('processed', 'cancel'),
                ('cancel', 'draft'),
                ))

        cls._buttons.update({
            'get_lines_to_pay': {
                'invisible': Eval('state') != 'draft',
                },
            'generate_vouchers': {
                'invisible': Eval('state') != 'processed',
                },
        })

    @classmethod
    @ModelView.button
    @Workflow.transition('cancel')
    def cancel(cls, records):
        for record in records:
            if record.lines:
                for line in record.lines:
                    if line.voucher:
                        line.voucher.cancel([line.voucher])
                        line.write([line], {'voucher': None})


    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, records):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('processed')
    def process(cls, records):
        for record in records:
            if not record.code:
                cls.set_number(record)

    @classmethod
    @ModelView.button
    def generate_vouchers(cls, records):
        for record in records:
            record.validate_vouchers()
            record.create_voucher(record.lines)

    @classmethod
    @ModelView.button
    def get_lines_to_pay(cls, records):
        for record in records:
            if record.lines:
                record.validate_vouchers()
                record.delete_lines()
            MoveLine = Pool().get('account.move.line')
            MultiRevenueLines = Pool().get('account.multirevenue.line')
            movelines = MoveLine.search([
                ('move.state', '=', 'posted'),
                ('move.company', '=', record.company.id),
                ('party', '=', record.party.id),
                ('state', '=', 'valid'),
                ('account.reconcile', '=', True),
                ('account.kind', '=', 'receivable'),
                ('reconciliation', '=', None),
            ], order=[('move.date', 'ASC')])
            lines_to_create = []
            if movelines:
                for line in movelines:
                    sum_lines = 0
                    multirevenue_lines = MultiRevenueLines.search([
                        ('move_line', '=', line.id)
                    ])
                    if multirevenue_lines:
                        sum_lines = sum([line.amount for line in multirevenue_lines if line.amount and line.state == 'processed'])
                    if line.origin and line.origin.__name__ == 'account.invoice':
                        amount_to_pay_ = line.origin.amount_to_pay
                    else:
                        amount_to_pay_ = line.amount
                    amount_to_pay_ = amount_to_pay_ - sum_lines
                    if amount_to_pay_ <= 0:
                        continue
                    lines_to_create.append(
                        {
                            'multirevenue': record.id,
                            'move_line': line,
                            'reference_document': line.reference or '',
                            'amount_to_pay': amount_to_pay_,
                            'amount': 0,
                            'payment_mode': None,
                            'date': None,
                            'transaction_origin': None,
                        }
                    )
                MultiRevenueLines.create(lines_to_create)

    @classmethod
    def copy(cls, records, default=None):
        if default is None:
            default = {}
        default = default.copy()
        default['lines'] = None
        default['state'] = 'draft'
        default['code'] = None
        return super(MultiRevenue, cls).copy(records, default=default)

    def delete_lines(self):
        MultiRevenueLines = Pool().get('account.multirevenue.line')
        MultiRevenueLines.delete(self.lines)

    def validate_vouchers(self):
        if self.lines:
            for line in self.lines:
                if line.voucher:
                    self.raise_user_error('exist_voucher')

    def get_total_transaction(self, name):
        amount_transaction = []
        if self.transactions:
            for tr in self.transactions:
                amount_transaction.append(tr.amount)
            return sum(amount_transaction)
        else:
            return 0

    def create_payments(self):
        if self.lines:
            self.validate_vouchers()
        else:
            return

        amount_ = 0
        for transaction in self.transactions:
            i = self.transactions.index(transaction)
            amount_ = transaction.amount
            self.compute_payment_lines(amount_, transaction)
            if i == (len(self.transactions) - 1) and self.new_balance < 0:
                self.create_new_line(None, abs(self.new_balance), Decimal('0.00'), transaction)

    def compute_payment_lines(self, amount_, transaction):
        for line in self.lines:
            if line.amount:
                continue
            if amount_ >= line.amount_to_pay:
                amount_ -= line.amount_to_pay
                value_ = line.amount_to_pay
                self.update_line(line, value_, transaction)
            else:
                value_ = amount_
                amount_ = 0
                self.update_line(line, value_, transaction)
                new_balance = line.amount_to_pay - value_
                _line_ = self.create_new_line(line.move_line.id, 0, new_balance, None)

            if amount_ == 0:
                break


    def update_line(self, line, value, transaction):
        transaction_id = transaction.id if transaction else None
        line.write([line], {
                'amount': value,
                'payment_mode': transaction.payment_mode.id,
                'date': transaction.date,
                'transaction_origin': transaction_id,
            }
        )

    def create_new_line(self, move_line_id, amount_, amount_to_pay,
                        transaction):
        MultiRevenueLine = Pool().get('account.multirevenue.line')
        transaction_id = transaction.id if transaction else None
        create_line = {
                'multirevenue': self.id,
                'move_line': move_line_id,
                'amount': amount_,
                'amount_to_pay': amount_to_pay,
                'payment_mode': None,
                'date': None,
                'is_prepayment': False,
                'transaction_origin': transaction_id,
            }
        if transaction:
            create_line['payment_mode'] = transaction.payment_mode.id
            create_line['date'] = transaction.date
            create_line['is_prepayment'] = True
        line, = MultiRevenueLine.create([create_line])
        return line

    def create_voucher(self, lines):
        voucher_to_create = {}
        for line in lines:
            if not line.transaction_origin:
                continue
            pool = Pool()
            Voucher = pool.get('account.voucher')
            MoveLine = pool.get('account.move.line')
            MultiRevenueLine = pool.get('account.multirevenue.line')
            voucher_type = 'receipt'
            payment_mode = line.transaction_origin.payment_mode

            if line.transaction_origin.id not in voucher_to_create.keys():
                voucher_to_create[line.transaction_origin.id] = {
                    'party': self.party.id,
                    'company': line.multirevenue.company.id,
                    'voucher_type': voucher_type,
                    'date': line.transaction_origin.date,
                    'description': '',
                    'reference': '',
                    'payment_mode': payment_mode.id,
                    'state': 'draft',
                    'account': payment_mode.account.id,
                    'journal': payment_mode.journal.id,
                    'lines': [('create', [])],
                    'method_counterpart': 'one_line',
                }

            if payment_mode.bank_account and payment_mode.bank_account:
                voucher_to_create[line.transaction_origin.id]['bank'] = payment_mode.bank_account.bank.id

            if line.move_line:
                move_line = line.move_line
                voucher_to_create[line.transaction_origin.id]['description'] = 'PAGO DE FACTURAS'
                voucher_to_create[line.transaction_origin.id]['reference'] = line.reference_document or ''

                if move_line.reconciliation:
                    continue
                origin_number = line.origin.number if line.origin else None
                total_amount =  line.origin.total_amount if line.origin and line.origin.__name__ == 'account.invoice' else move_line.amount
                voucher_to_create[line.transaction_origin.id]['lines'][0][1].append({
                    'detail': origin_number or move_line.reference or move_line.description,
                    'amount': line.amount,
                    'amount_original': total_amount,
                    'move_line': move_line.id,
                    'account': move_line.account.id,
                })
            else:
                account_id = line.account.id if line.account else None
                detail_ = line.reference_document
                if line.is_prepayment:
                    detail_ = 'ANTICIPO'
                    Config = pool.get('account.voucher_configuration')
                    config = Config(1)
                    if not account_id:
                        if config and not config.account_prepayment:
                            self.raise_user_error('without_account_prepayment')
                        account_id = config.account_prepayment.id

                voucher_to_create[line.transaction_origin.id]['lines'][0][1].append({
                    'detail': detail_,
                    'amount': line.amount,
                    'account':  account_id,
                })
        for key in voucher_to_create.keys():
            vouchers = Voucher.create([voucher_to_create[key]])
            for voucher in vouchers:
                voucher.on_change_lines()
                voucher.save()
                line_mult = MultiRevenueLine.search([
                    ('transaction_origin', '=', key)
                ])
                if line_mult:
                    MultiRevenueLine.write(line_mult, {'voucher': voucher.id})
            Voucher.process(vouchers)
        # return vouchers


    def get_total_amount_line(self, name):
        if self.lines:
            sum_amount = []
            for line in self.lines:
                if line.amount:
                    sum_amount.append(line.amount)
                else:
                    sum_amount.append(line.balance)
            return sum(sum_amount)

    def get_new_balance(self, name):
        if self.total_lines_to_pay and self.total_transaction:
            return self.total_lines_to_pay - self.total_transaction

    def get_total_line_receipt(self, name):
        if self.lines:
            return sum([line.amount for line in self.lines if line.amount])
        else:
            return 0

    @staticmethod
    def default_state():
        return 'draft'

    @staticmethod
    def default_company():
        return Transaction().context.get('company') or None

    @classmethod
    def validate(cls, records):
        pass

    @classmethod
    def set_number(cls, request):
        '''
        Fill the number field with the MultiRevenue sequence
        '''
        pool = Pool()
        Sequence = pool.get('ir.sequence')
        Config = pool.get('account.voucher_configuration')
        config = Config.search([
            ('company', '=', request.company.id)
        ])
        if config:
            if not config[0].multirevenue_sequence:
                return
            number = Sequence.get_id(config[0].multirevenue_sequence.id)
            cls.write([request], {'code': number})

    @classmethod
    def delete(cls, records):
        for record in records:
            record.validate_vouchers()
        return super(MultiRevenue, cls).delete(records)


class PaymentLines(Wizard):
    'Payment Lines'
    __name__ = 'account.multirevenue.payment_lines'
    start_state = 'payment_lines'
    payment_lines = StateTransition()

    def transition_payment_lines(self):
        MultiRevenue = Pool().get('account.multirevenue')
        ids = Transaction().context['active_ids']
        for mr in MultiRevenue.browse(ids):
            if mr.state != 'draft':
                return 'end'
            mr.create_payments()
        return 'end'


class MultiRevenueTransaction(ModelSQL, ModelView):
    'MultiRevenue Transaction'
    __name__ = 'account.multirevenue.transaction'
    _rec_name = 'amount'
    multirevenue = fields.Many2One('account.multirevenue', 'Multirevenue',
        required=True, select=True)
    description = fields.Char('Description')
    amount = fields.Numeric('Amount', required=True)
    date = fields.Date('Date', required=True)
    payment_mode = fields.Many2One('account.voucher.paymode', 'Payment Mode', required=True, domain=[
        ('company', '=', Eval('context', {}).get('company', -1)),
    ])
    state = fields.Function(fields.Char('State', readonly=True), 'get_state')

    @classmethod
    def __up__(cls):
        super(MultiRevenueTransaction, cls).__setup__()
        cls._order.insert(0, ('date', 'ASC')),

    @classmethod
    def delete(cls, records):
        for record in records:
            record.multirevenue.validate_vouchers()
        return super(MultiRevenueTransaction, cls).delete(records)

    def get_state(self, name):
        if self.multirevenue:
            return self.multirevenue.state

class MultiRevenueLine(ModelSQL, ModelView):
    'MultiRevenue Line'
    __name__ = 'account.multirevenue.line'
    multirevenue = fields.Many2One('account.multirevenue', 'Multirevenue',
        required=True, select=True)
    move_line = fields.Many2One('account.move.line', 'Move Line', domain=[
            ('move.state', '=', 'posted'),
            ('move.company', '=', Eval('context', {}).get('company', -1)),
            ('state', '=', 'valid'),
            ('account.reconcile', '=', True),
            ('account.kind', '=', 'receivable'),
            ('reconciliation', '=', None),
    ], depends=['party_document'])
    date_document = fields.Function(fields.Date('Date Document', readonly=True), 'get_data')
    reference_document = fields.Char('Reference')
    party_document = fields.Function(fields.Many2One('party.party',  'Party',  readonly=True), 'get_party')
    amount_to_pay = fields.Numeric('Amount to Pay', readonly=True)
    balance = fields.Function(fields.Numeric('Balance', readonly=True), 'get_balance')
    amount = fields.Numeric('Amount')
    payment_mode = fields.Many2One('account.voucher.paymode', 'Payment Mode', domain=[
        ('company', '=', Eval('context', {}).get('company', -1)),
    ])
    date = fields.Date('Date',)
    voucher = fields.Many2One('account.voucher', 'Voucher', readonly=True)
    is_prepayment = fields.Boolean('is_prepayment',)
    transaction_origin = fields.Many2One('account.multirevenue.transaction', 'Transaction Origin',  domain=[
        ('multirevenue', '=', Eval('multirevenue')),
    ], depends=['multirevenue'],  states={
            'required': Bool(Eval('amount')),
            })
    origin = fields.Function(fields.Reference('Origin', selection='get_origin_selection',  readonly=True), 'get_origin')
    state = fields.Function(fields.Char('State', readonly=True), 'get_state')
    # is_discount = fields.Boolean('is_discount')
    account = fields.Many2One('account.account', 'Account', states={
            'required': Not(Bool(Eval('move_line'))),
            'invisible': Bool(Eval('move_line')),
            }, domain=[
                ('company', '=', Eval('context', {}).get('company', -1)),
                ('kind', '!=', 'view'),
            ])

    @classmethod
    def __setup__(cls):
        super(MultiRevenueLine, cls).__setup__()
        cls._order.insert(0, ('move_line', 'ASC'))

    @classmethod
    def delete(cls, records):
        for record in records:
            record.multirevenue.validate_vouchers()
        return super(MultiRevenueLine, cls).delete(records)

    def get_data(self, name):
        if name == 'date_document' and self.move_line:
            return self.move_line.move.date
        # if name == 'reference_document' and self.move_line:
        #     return self.move_line.reference or ''

    # @fields.depends('payment_mode', 'date' 'transaction_origin')
    # def on_change_transaction_origin(self):
    #     if self.transaction_origin:
    #         self.date = self.transaction_origin.date
    #         self.payment_mode = self.transaction_origin.payment_mode.id

    def get_state(self, name):
        if self.multirevenue:
            return self.multirevenue.state

    def get_party(self, name):
        if self.multirevenue and self.multirevenue.party:
            return self.multirevenue.party.id

    def get_balance(self, name):
        if self.amount and self.amount_to_pay:
            return abs(self.amount_to_pay - self.amount)
        else:
            return abs(self.amount_to_pay)

    @classmethod
    def get_origin_selection(cls):
        Move = Pool().get('account.move')
        return Move.get_origin()

    def get_origin(self, name):
        if self.move_line and self.move_line.origin:
            return str(self.move_line.origin)

    @staticmethod
    def default_amount_to_pay():
        return 0

    @staticmethod
    def default_is_prepayment():
        return False


class MultiRevenueReport(Report):
    'MultiRevenue Report'
    __name__ = 'account.multirevenue.report'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(MultiRevenueReport, cls).get_context(records, data)
        Company = Pool().get('company.company')
        company_id = Transaction().context.get('company')
        report_context['company'] = Company(company_id)
        return report_context
